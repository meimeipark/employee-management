package com.pym.employeemanagermentapi.model;

import com.pym.employeemanagermentapi.enums.Department;
import com.pym.employeemanagermentapi.enums.EmploymentType;
import com.pym.employeemanagermentapi.enums.Job;
import com.pym.employeemanagermentapi.enums.Position;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class EmployeeRequest {
    private String employeeId;
    @Enumerated(value = EnumType.STRING)
    private Department department;
    private String name;
    @Enumerated(value = EnumType.STRING)
    private Position position;
    @Enumerated(value = EnumType.STRING)
    private Job job;
    @Enumerated(value = EnumType.STRING)
    private EmploymentType employmentType;
    private LocalDate birthDate;
    private String phoneNumber;
    private String address;
    private Integer income;
    private LocalDate joinDay;
    private LocalDate endDay;
}
