package com.pym.employeemanagermentapi.model;

import com.pym.employeemanagermentapi.enums.EmploymentType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class EmployeeItem {
    private Long id;
    private String employeeId;
    private String department;
    private String name;
    private String position;
    private String employmentType;
    private LocalDate birthDate;
    private String phoneNumber;
    private String address;
    private LocalDate joinDay;
    private LocalDate endDay;
}
