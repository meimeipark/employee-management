package com.pym.employeemanagermentapi.service;

import com.pym.employeemanagermentapi.entity.Employee;
import com.pym.employeemanagermentapi.model.EmployeeItem;
import com.pym.employeemanagermentapi.model.EmployeeRequest;
import com.pym.employeemanagermentapi.model.EmployeeResponse;
import com.pym.employeemanagermentapi.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    public void setEmployee(EmployeeRequest request){
        Employee addData = new Employee();
        addData.setEmployeeId(request.getEmployeeId());
        addData.setDepartment(request.getDepartment());
        addData.setName(request.getName());
        addData.setPosition(request.getPosition());
        addData.setJob(request.getJob());
        addData.setEmploymentType(request.getEmploymentType());
        addData.setBirthDate(request.getBirthDate());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setAddress(request.getAddress());
        addData.setIncome(request.getIncome());
        addData.setJoinDay(request.getJoinDay());
        addData.setEndDay(request.getEndDay());

        employeeRepository.save(addData);
    }

    public List<EmployeeItem> getEmployees(){
        List<Employee> orginList = employeeRepository.findAll();

        List<EmployeeItem> result = new LinkedList<>();

        for (Employee employee: orginList){
            EmployeeItem addItem = new EmployeeItem();
            addItem.setId(employee.getId());
            addItem.setEmployeeId(employee.getEmployeeId());
            addItem.setDepartment(employee.getDepartment().getName());
            addItem.setName(employee.getName());
            addItem.setPosition(employee.getPosition().getName());
            addItem.setEmploymentType(employee.getEmploymentType().getName());
            addItem.setBirthDate(employee.getBirthDate());
            addItem.setPhoneNumber(employee.getPhoneNumber());
            addItem.setAddress(employee.getAddress());
            addItem.setJoinDay(employee.getJoinDay());
            addItem.setEndDay(employee.getEndDay());

            result.add(addItem);
        }
        return result;
    }

    public EmployeeResponse getEmployee(long id){
        Employee originData = employeeRepository.findById(id).orElseThrow();

        EmployeeResponse response = new EmployeeResponse();
        response.setId(originData.getId());
        response.setEmployeeId(originData.getEmployeeId());
        response.setDepartment(originData.getDepartment().getName());
        response.setName(originData.getName());
        response.setPosition(originData.getPosition().getName());
        response.setJob(originData.getJob().getName());
        response.setEmploymentType(originData.getEmploymentType().getName());
        response.setBirthDate(originData.getBirthDate());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setAddress(originData.getAddress());
        response.setIncome(originData.getIncome());
        response.setJoinDay(originData.getJoinDay());
        response.setEndDay(originData.getEndDay());

        return response;
    }
}
