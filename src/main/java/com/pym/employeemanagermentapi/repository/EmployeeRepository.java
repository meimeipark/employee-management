package com.pym.employeemanagermentapi.repository;

import com.pym.employeemanagermentapi.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository <Employee, Long> {
}
