package com.pym.employeemanagermentapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmployeeManagermentApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeeManagermentApiApplication.class, args);
	}

}
