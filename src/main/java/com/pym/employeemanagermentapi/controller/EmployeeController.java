package com.pym.employeemanagermentapi.controller;

import com.pym.employeemanagermentapi.model.EmployeeItem;
import com.pym.employeemanagermentapi.model.EmployeeRequest;
import com.pym.employeemanagermentapi.model.EmployeeResponse;
import com.pym.employeemanagermentapi.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/employee")
public class EmployeeController {
    private final EmployeeService employeeService;

    @PostMapping("/new")
    public String setEmployee(@RequestBody EmployeeRequest request){
        employeeService.setEmployee(request);

        return "등록완료";
    }

    @GetMapping("/list")
    public List<EmployeeItem> getEmployees(){
        return employeeService.getEmployees();
    }

    @GetMapping("/detail/{id}")
    public EmployeeResponse employeeResponse(@PathVariable long id){
        return employeeService.getEmployee(id);
    }
}
