package com.pym.employeemanagermentapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EmploymentType {
    PERMANENT("정규직"),
    CONTRACT("계약직");

    private final String name;
}
