package com.pym.employeemanagermentapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@AllArgsConstructor
public enum Department {
    SALES("영업팀"),
    ACCOUNTING("경리부"),
    TECHNICAL_SUPPORT("기술지원팀"),
    RD("개발팀");

    private final String name;
}
