package com.pym.employeemanagermentapi.entity;

import com.pym.employeemanagermentapi.enums.Department;
import com.pym.employeemanagermentapi.enums.EmploymentType;
import com.pym.employeemanagermentapi.enums.Job;
import com.pym.employeemanagermentapi.enums.Position;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 30)
    private String employeeId;

    @Column(nullable = false, length = 30)
    @Enumerated(value = EnumType.STRING)
    private Department department;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private Position position;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private Job job;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private EmploymentType employmentType;

    @Column(nullable = false)
    private LocalDate birthDate;

    @Column(nullable = false, length = 20)
    private String phoneNumber;

    @Column(nullable = false)
    private String address;

    @Column(nullable = false)
    private Integer income;

    @Column(nullable = false)
    private LocalDate joinDay;

    private LocalDate endDay;
}
